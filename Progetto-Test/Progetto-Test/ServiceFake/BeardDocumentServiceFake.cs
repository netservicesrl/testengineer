﻿using Progetto_Test_WebAPI.Models;
using Progetto_Test_WebAPI.Models.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Progetto_Test
{
    public class BeardDocumentServiceFake : IBeardService<DocumentItem>
    {
        private readonly List<DocumentItem> _documents;
        public BeardDocumentServiceFake()
        {
            _documents = new List<DocumentItem>()
            {
                 new DocumentItem() {
                    Id = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200"),
                    Description = "Documento d’identità dei cittadini italiani",
                    Title = "Carta d'identità",
                    FileName = "carta-identità-elettronica.jpg"
                },
                  new DocumentItem() {
                    Id = new Guid("815accac-fd5b-478a-a9d6-f171a2f6ae7f"),
                    Description = "Un manuale per barbieri",
                    Title = "Manuale giovane barbiere",
                    FileName = "BARBER-MAGAZINE-GRATUITO.pdf"
                  }
            };
        }
        public IEnumerable<DocumentItem> GetAllItems()
        {
            return _documents;
        }

        public DocumentItem Add(DocumentItem newItem)
        {
            newItem.Id = Guid.NewGuid();
            _documents.Add(newItem);
            return newItem;
        }

        public DocumentItem GetById(Guid id)
        {
            return _documents.Where(a => a.Id == id)
                .FirstOrDefault();
        }

        public void Remove(Guid id)
        {
            var existing = _documents.First(a => a.Id == id);
            _documents.Remove(existing);
        }
    }
}
