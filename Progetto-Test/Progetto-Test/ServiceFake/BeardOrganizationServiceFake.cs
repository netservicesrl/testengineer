﻿using Progetto_Test_WebAPI.Models;
using Progetto_Test_WebAPI.Models.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Progetto_Test
{
    public class BeardOrganizationServiceFake : IBeardService<OrganizationItem>
    {
        private readonly List<OrganizationItem> _organizations;

        public BeardOrganizationServiceFake()
        {
            _organizations = new List<OrganizationItem>()
            {
                new OrganizationItem() { 
                    Id = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200"),
                    Name = "Lubowitz",
                    Addresses=new List<string>(){"0052 Gerlach Islands", "595 Parker Route" },
                    Contacts=new List<string>(){ "Caden_Gerhold@yahoo.com" , "Bernita86@yahoo.com" },
                    EmployeeCount = 52
                },
                new OrganizationItem() { 
                    Id = new Guid("815accac-fd5b-478a-a9d6-f171a2f6ae7f"),
                   Name = "Schowalter - O'Hara",
                    Addresses=new List<string>(){"123 Erika Street" },
                    Contacts=new List<string>(){ "Kristian_Johnson@yahoo.com" },
                    EmployeeCount = 23
                },
                new OrganizationItem() { 
                    Id = new Guid("33704c4a-5b87-464c-bfb6-51971b4d18ad"),
                    Name = "Ryan - Lowe",
                    Addresses=new List<string>(){"306 Gunner Radial" },
                    Contacts=new List<string>(){ "Godfrey_Breitenberg84@gmail.com" },
                    EmployeeCount = 31
                }
            };
        }

        public IEnumerable<OrganizationItem> GetAllItems()
        {
            return _organizations;
        }

        public OrganizationItem Add(OrganizationItem newItem)
        {
            newItem.Id = Guid.NewGuid();
            _organizations.Add(newItem);
            return newItem;
        }

        public OrganizationItem GetById(Guid id)
        {
            return _organizations.Where(a => a.Id == id)
                .FirstOrDefault();
        }

        public void Remove(Guid id)
        {
            var existing = _organizations.First(a => a.Id == id);
            _organizations.Remove(existing);
        }
    }
}
