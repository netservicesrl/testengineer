﻿using Progetto_Test_WebAPI.Models;
using Progetto_Test_WebAPI.Models.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Progetto_Test
{
    public class BeardEmployeeServiceFake : IBeardService<EmployeeItem>
    {
        private readonly List<EmployeeItem> _employees;
        public BeardEmployeeServiceFake()
        {
            _employees = new List<EmployeeItem>()
            {
                 new EmployeeItem() {
                    Id = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200"),
                    Name = "Lubowitz",
                    Surame = "jkhdfhjgjdh",
                    FiscalCode="fiscalcode 1",
                    Email="Caden_Gerhold@yahoo.com"
                },
                  new EmployeeItem() {
                    Id = new Guid("815accac-fd5b-478a-a9d6-f171a2f6ae7f"),
                   Name = "Schowalter ",
                   Surame = "O'Hara",
                    FiscalCode="123 Erika Street" ,
                    Email="Kristian_Johnson@yahoo.com"

                },
                new EmployeeItem() {
                    Id = new Guid("33704c4a-5b87-464c-bfb6-51971b4d18ad"),
                    Name = "Ryan ",
                    Surame= "Lowe",
                    FiscalCode="306 Gunner Radial" ,
                    Email="Godfrey_Breitenberg84@gmail.com" ,

                }
            };
        }
        public IEnumerable<EmployeeItem> GetAllItems()
        {
            return _employees;
        }

        public EmployeeItem Add(EmployeeItem newItem)
        {
            newItem.Id = Guid.NewGuid();
            _employees.Add(newItem);
            return newItem;
        }

        public EmployeeItem GetById(Guid id)
        {
            return _employees.Where(a => a.Id == id)
                .FirstOrDefault();
        }

        public void Remove(Guid id)
        {
            var existing = _employees.First(a => a.Id == id);
            _employees.Remove(existing);
        }
    }
}
