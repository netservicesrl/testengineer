﻿using Microsoft.AspNetCore.Mvc;
using Progetto_Test_WebAPI.Controller;
using Progetto_Test_WebAPI.Models;
using Progetto_Test_WebAPI.Models.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Progetto_Test
{
    public class BeardDocumentControllerTest
    {
        private readonly BeardDocumentController _controller;
        private readonly IBeardService<DocumentItem> _service;
        public BeardDocumentControllerTest()
        {
            _service = new BeardDocumentServiceFake();

            _controller = new BeardDocumentController(_service);
        }

        #region Testing the Get Method
        [Fact]

        public void Get_WhenCalled_ReturnsOkResult()

        {

            // Act

            var okResult = _controller.Get();

            // Assert

            Assert.IsType<OkObjectResult>(okResult as OkObjectResult);

        }

        [Fact]

        public void Get_WhenCalled_ReturnsAllItems()

        {

            // Act

            var okResult = _controller.Get() as OkObjectResult;

            // Assert

            var items = Assert.IsType<List<DocumentItem>>(okResult.Value);

            Assert.Equal(2, items.Count);

        }
        #endregion

        #region  Testing the GetById method 
        [Fact]
        public void GetById_UnknownGuidPassed_ReturnsNotFoundResult()
        {
            // Act
            var notFoundResult = _controller.Get(Guid.NewGuid());

            // Assert
            Assert.IsType<NotFoundResult>(notFoundResult);
        }

        [Fact]
        public void GetById_ExistingGuidPassed_ReturnsOkResult()
        {
            // Arrange
            var testGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");

            // Act
            var okResult = _controller.Get(testGuid);

            // Assert
            Assert.IsType<OkObjectResult>(okResult as OkObjectResult);
        }

        [Fact]
        public void GetById_ExistingGuidPassed_ReturnsRightItem()
        {
            // Arrange
            var testGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");

            // Act
            var okResult = _controller.Get(testGuid) as OkObjectResult;

            // Assert
            Assert.IsType<DocumentItem>(okResult.Value);
            Assert.Equal(testGuid, (okResult.Value as DocumentItem).Id);
        }

        #endregion

        #region Testing the Add Method
        [Fact]
        public void Add_InvalidObjectPassed_ReturnsBadRequest()
        {
            // Arrange
            var nameMissingItem = new DocumentItem()
            {
               
                Title = "Giornaletto barbiere",
                Description = "Un giornaletto bello"

            };
            _controller.ModelState.AddModelError("FileName", "Required");

            // Act
            var badResponse = _controller.Post(nameMissingItem);

            // Assert
            Assert.IsType<BadRequestObjectResult>(badResponse);
        }

        [Fact]
        public void Add_ValidObjectPassed_ReturnsCreatedResponse()
        {
            // Arrange
            DocumentItem testItem = new DocumentItem()
            {
                Title = "Giornaletto barbiere",
                Description = "Un giornaletto bello",
                FileName = "tipi-barba-e1595753875445.png",
                Url = new Uri(
                    "https://www.tagliacapelliuomo.it/wp-content/uploads/2020/07/tipi-barba-e1595753875445.png")
            };

            // Act
            var createdResponse = _controller.Post(testItem);

            // Assert
            Assert.IsType<CreatedAtActionResult>(createdResponse);
        }

        [Fact]
        public void Add_ValidObjectPassed_ReturnedResponseHasCreatedItem()
        {
            // Arrange
            var testItem = new DocumentItem()
            {
                Title = "Attestato da barbiere",
                Description = "Attesta che sono barbiere",
                FileName = "attestati.jpg",
                Url = new Uri(
                    "https://www.maxjobarbershop.it/wp-content/uploads/2015/01/attestati.jpg")
            };

            // Act
            var createdResponse = _controller.Post(testItem) as CreatedAtActionResult;
            var item = createdResponse.Value as DocumentItem;

            // Assert
            Assert.IsType<DocumentItem>(item);
            Assert.Equal("attestati.jpg", item.FileName);
        }
        #endregion

        #region  Testing Remove method
        [Fact]
        public void Remove_NotExistingGuidPassed_ReturnsNotFoundResponse()
        {
            // Arrange
            var notExistingGuid = Guid.NewGuid();

            // Act
            var badResponse = _controller.Remove(notExistingGuid);

            // Assert
            Assert.IsType<NotFoundResult>(badResponse);
        }

        [Fact]
        public void Remove_ExistingGuidPassed_ReturnsNoContentResult()
        {
            // Arrange
            var existingGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");

            // Act
            var noContentResponse = _controller.Remove(existingGuid);

            // Assert
            Assert.IsType<NoContentResult>(noContentResponse);
        }

        [Fact]
        public void Remove_ExistingGuidPassed_RemovesOneItem()
        {
            // Arrange
            var existingGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");

            // Act
            var okResponse = _controller.Remove(existingGuid);

            // Assert
            Assert.Equal(1, _service.GetAllItems().Count());
        }
        #endregion
    }
}
