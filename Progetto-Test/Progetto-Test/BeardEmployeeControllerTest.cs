﻿using Microsoft.AspNetCore.Mvc;
using Progetto_Test_WebAPI.Controller;
using Progetto_Test_WebAPI.Models;
using Progetto_Test_WebAPI.Models.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Progetto_Test
{
    public class BeardEmployeeControllerTest
    {
        private readonly BeardEmployeeController _controller;
        private readonly IBeardService<EmployeeItem> _service;
        public BeardEmployeeControllerTest()
        {
            _service = new BeardEmployeeServiceFake();

            _controller = new BeardEmployeeController(_service);
        }

        #region Testing the Get Method
        [Fact]

        public void Get_WhenCalled_ReturnsOkResult()

        {

            // Act

            var okResult = _controller.Get();

            // Assert

            Assert.IsType<OkObjectResult>(okResult as OkObjectResult);

        }

        [Fact]

        public void Get_WhenCalled_ReturnsAllItems()

        {

            // Act

            var okResult = _controller.Get() as OkObjectResult;

            // Assert

            var items = Assert.IsType<List<EmployeeItem>>(okResult.Value);

            Assert.Equal(3, items.Count);

        }
        #endregion

        #region  Testing the GetById method 
        [Fact]
        public void GetById_UnknownGuidPassed_ReturnsNotFoundResult()
        {
            // Act
            var notFoundResult = _controller.Get(Guid.NewGuid());

            // Assert
            Assert.IsType<NotFoundResult>(notFoundResult);
        }

        [Fact]
        public void GetById_ExistingGuidPassed_ReturnsOkResult()
        {
            // Arrange
            var testGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");

            // Act
            var okResult = _controller.Get(testGuid);

            // Assert
            Assert.IsType<OkObjectResult>(okResult as OkObjectResult);
        }

        [Fact]
        public void GetById_ExistingGuidPassed_ReturnsRightItem()
        {
            // Arrange
            var testGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");

            // Act
            var okResult = _controller.Get(testGuid) as OkObjectResult;

            // Assert
            Assert.IsType<EmployeeItem>(okResult.Value);
            Assert.Equal(testGuid, (okResult.Value as EmployeeItem).Id);
        }

        #endregion

        #region Testing the Add Method
        [Fact]
        public void Add_InvalidObjectPassed_ReturnsBadRequest()
        {
            // Arrange
            var nameMissingItem = new EmployeeItem()
            {
               
                Email = "fff@gg.it" 
               
            };
            _controller.ModelState.AddModelError("Name", "Required");
            _controller.ModelState.AddModelError("Surname", "Required");
            _controller.ModelState.AddModelError("FiscalCode", "Required");

            // Act
            var badResponse = _controller.Post(nameMissingItem);

            // Assert
            Assert.IsType<BadRequestObjectResult>(badResponse);
        }

        [Fact]
        public void Add_ValidObjectPassed_ReturnsCreatedResponse()
        {
            // Arrange
            EmployeeItem testItem = new EmployeeItem()
            {
                Name = "Heineken",
                Surame = "cognome",
                FiscalCode =  "dddfff56l23e156f" ,
                Email =  "ggg@gg.it"
            };

            // Act
            var createdResponse = _controller.Post(testItem);

            // Assert
            Assert.IsType<CreatedAtActionResult>(createdResponse);
        }

        [Fact]
        public void Add_ValidObjectPassed_ReturnedResponseHasCreatedItem()
        {
            // Arrange
            var testItem = new EmployeeItem()
            {
                Name = "Heineken",
                Surame = "hgsahgfdfh",
                FiscalCode = "aaafff56l23e156f",
                Email = "aaa@gg.it"
            };

            // Act
            var createdResponse = _controller.Post(testItem) as CreatedAtActionResult;
            var item = createdResponse.Value as EmployeeItem;

            // Assert
            Assert.IsType<EmployeeItem>(item);
            Assert.Equal("Heineken", item.Name);
        }
        #endregion

        #region  Testing Remove method
        [Fact]
        public void Remove_NotExistingGuidPassed_ReturnsNotFoundResponse()
        {
            // Arrange
            var notExistingGuid = Guid.NewGuid();

            // Act
            var badResponse = _controller.Remove(notExistingGuid);

            // Assert
            Assert.IsType<NotFoundResult>(badResponse);
        }

        [Fact]
        public void Remove_ExistingGuidPassed_ReturnsNoContentResult()
        {
            // Arrange
            var existingGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");

            // Act
            var noContentResponse = _controller.Remove(existingGuid);

            // Assert
            Assert.IsType<NoContentResult>(noContentResponse);
        }

        [Fact]
        public void Remove_ExistingGuidPassed_RemovesOneItem()
        {
            // Arrange
            var existingGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");

            // Act
            var okResponse = _controller.Remove(existingGuid);

            // Assert
            Assert.Equal(2, _service.GetAllItems().Count());
        }
        #endregion
    }
}
