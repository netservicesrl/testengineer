using Microsoft.AspNetCore.Mvc;
using Progetto_Test_WebAPI.Controller;
using Progetto_Test_WebAPI.Models;
using Progetto_Test_WebAPI.Models.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Progetto_Test
{
    public class BeardOrganizationControllerTest
    {
        private readonly BeardOrganizationController _controller;
        private readonly IBeardService<OrganizationItem> _service;
        public BeardOrganizationControllerTest()
        {
            _service = new BeardOrganizationServiceFake();

            _controller = new BeardOrganizationController(_service);
        }

        #region Testing the Get Method
        [Fact]

        public void Get_WhenCalled_ReturnsOkResult()

        {

            // Act

            var okResult = _controller.Get();

            // Assert

            Assert.IsType<OkObjectResult>(okResult as OkObjectResult);

        }

        [Fact]

        public void Get_WhenCalled_ReturnsAllItems()

        {

            // Act

            var okResult = _controller.Get() as OkObjectResult;

            // Assert

            var items = Assert.IsType<List<OrganizationItem>>(okResult.Value);

            Assert.Equal(3, items.Count);

        }
        #endregion


        #region  Testing the GetById method 
        [Fact]
        public void GetById_UnknownGuidPassed_ReturnsNotFoundResult()
        {
            // Act
            var notFoundResult = _controller.Get(Guid.NewGuid());

            // Assert
            Assert.IsType<NotFoundResult>(notFoundResult);
        }

        [Fact]
        public void GetById_ExistingGuidPassed_ReturnsOkResult()
        {
            // Arrange
            var testGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");

            // Act
            var okResult = _controller.Get(testGuid);

            // Assert
            Assert.IsType<OkObjectResult>(okResult as OkObjectResult);
        }

        [Fact]
        public void GetById_ExistingGuidPassed_ReturnsRightItem()
        {
            // Arrange
            var testGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");

            // Act
            var okResult = _controller.Get(testGuid) as OkObjectResult;

            // Assert
            Assert.IsType<OrganizationItem>(okResult.Value);
            Assert.Equal(testGuid, (okResult.Value as OrganizationItem).Id);
        }

        #endregion

        #region Testing the Add Method
        [Fact]
        public void Add_InvalidObjectPassed_ReturnsBadRequest()
        {
            // Arrange
            var nameMissingItem = new OrganizationItem()
            {
                Addresses = new List<string> { "via piergianni,34" },
                Contacts = new List<string> { "fff@gg.it" },
                EmployeeCount = 3
            };
            _controller.ModelState.AddModelError("Name", "Required");

            // Act
            var badResponse = _controller.Post(nameMissingItem);

            // Assert
            Assert.IsType<BadRequestObjectResult>(badResponse);
        }

        [Fact]
        public void Add_ValidObjectPassed_ReturnsCreatedResponse()
        {
            // Arrange
            OrganizationItem testItem = new OrganizationItem()
            {
                Name = "Heineken Original 6 Pack",
                Addresses = new List<string> { "via pierpaolo,34" },
                Contacts = new List<string> { "ggg@gg.it" },
                EmployeeCount = 43
            };

            // Act
            var createdResponse = _controller.Post(testItem);

            // Assert
            Assert.IsType<CreatedAtActionResult>(createdResponse);
        }

        [Fact]
        public void Add_ValidObjectPassed_ReturnedResponseHasCreatedItem()
        {
            // Arrange
            var testItem = new OrganizationItem()
            {
                Name = "Guinness Original 6 Pack",
                Addresses = new List<string> { "via giocchino,34" },
                Contacts = new List<string> { "hhh@hh.it"},
                EmployeeCount = 25
            };

            // Act
            var createdResponse = _controller.Post(testItem) as CreatedAtActionResult;
            var item = createdResponse.Value as OrganizationItem;

            // Assert
            Assert.IsType<OrganizationItem>(item);
            Assert.Equal("Guinness Original 6 Pack", item.Name);
        }
        #endregion

        #region  Testing Remove method
        [Fact]
        public void Remove_NotExistingGuidPassed_ReturnsNotFoundResponse()
        {
            // Arrange
            var notExistingGuid = Guid.NewGuid();

            // Act
            var badResponse = _controller.Remove(notExistingGuid);

            // Assert
            Assert.IsType<NotFoundResult>(badResponse);
        }

        [Fact]
        public void Remove_ExistingGuidPassed_ReturnsNoContentResult()
        {
            // Arrange
            var existingGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");

            // Act
            var noContentResponse = _controller.Remove(existingGuid);

            // Assert
            Assert.IsType<NoContentResult>(noContentResponse);
        }

        [Fact]
        public void Remove_ExistingGuidPassed_RemovesOneItem()
        {
            // Arrange
            var existingGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");

            // Act
            var okResponse = _controller.Remove(existingGuid);

            // Assert
            Assert.Equal(2, _service.GetAllItems().Count());
        }

        #endregion
    }
}
