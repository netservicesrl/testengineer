﻿using Autofac.Extras.Moq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Progetto_Test_WebAPI.Controller;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow.Analytics.UserId;
using Xunit;

namespace Progetto_Test
{
    public class FileControllerTest
    {
        private readonly FileController _controller;

        public FileControllerTest()
        {

            _controller = new FileController();
        }

        [Fact(DisplayName = "Upload should read the contents from the file stream")]
        public async Task Upload_ReadsFileStream()
        {
            // Get a loose automock
            var Mock = AutoMock.GetLoose();
            string expectedFileContents = "This is the expected file contents!";
            // Set up the form file with a stream containing the expected file contents
            Mock<IFormFile> formFile = new Mock<IFormFile>();
            formFile.Setup(ff => ff.CopyToAsync(It.IsAny<Stream>(), It.IsAny<CancellationToken>()))
              .Returns<Stream, CancellationToken>((s, ct) =>
              {
                  byte[] buffer = Encoding.Default.GetBytes(expectedFileContents);
                  s.Write(buffer, 0, buffer.Length);
                  return Task.CompletedTask;
              });
            // Set up the form collection with the mocked form
            Mock<IFormCollection> forms = new Mock<IFormCollection>();
            forms.Setup(f => f.Files[It.IsAny<int>()]).Returns(formFile.Object);
            // Create the Upload Contoller
            var uploadController = Mock.Create<FileController>();
            // Set up the context
            uploadController.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
            };
            // Set up the forms
            uploadController.Request.Form = forms.Object;
            // Invoke the method
            await uploadController.UploadFiles(formFile.Object);
            // Verify that the File Service was called with the expected file contents
            //Mock.Mock<IFileService>().Verify(fs => fs.Import(expectedFileContents), Times.Once());
        }
    }
}
