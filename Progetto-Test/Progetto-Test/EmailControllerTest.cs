using Microsoft.AspNetCore.Mvc;
using Progetto_Test_WebAPI.Controller;
using Progetto_Test_WebAPI.Models;
using Progetto_Test_WebAPI.Models.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Xunit;

namespace Progetto_Test
{
    public class EmailControllerTest
    {
        private readonly EmailController _controller;
        private readonly EmailService _service;
        public EmailControllerTest()
        {
            _service = new EmailService();
            _controller = new EmailController(_service);
        }

        private bool IsValid(string emailAddress)
        {
            return Regex.IsMatch(emailAddress, @"^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$");
        }

        private EmailItem MakeMessage(string toAddress)
        {
            string Subject = "New employee";
            string Body = "NUOVO DIPENDENTE";
            return new EmailItem()
            {
                Body = Body,
                Subject = Subject,
                To = toAddress
            };
        }
        private EmailItem SupportMail(string toAddress)
        {
            if (IsValid(toAddress))
            {
                EmailItem message = MakeMessage(toAddress);
                return message;
            }
            return null;
        }

        #region Testing the SEND Method
        [Fact]
        public void Send_InvalidEmailPassed_ReturnsBadRequest()
        {
            // Arrange
            EmailItem _emailTest = SupportMail("hoyec65710runfons.com");
           
            // Act
            var badResponse = _controller.Send(_emailTest);

            // Assert
            Assert.IsType<BadRequestObjectResult>(badResponse);
        }

        [Fact]
        public void Send_ValiddEmailPassed_ReturnsCreatedResponse()
        {
            // Arrange
            EmailItem _emailTest = SupportMail("miranda.alessia88@gmail.com");

            // Act
            var createdResponse = _controller.Send(_emailTest);

            // Assert
            Assert.IsType<OkObjectResult>(createdResponse);
        }
        
        #endregion

    }
}
