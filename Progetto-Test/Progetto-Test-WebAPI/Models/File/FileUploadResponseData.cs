﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Progetto_Test_WebAPI.Models
{
    public class FileUploadResponseData
    {      
        public Guid Id { get; set; }

        public string Status { get; set; }
        public string FileName { get; set; }
        public string ErrorMessage { get; set; }
    }

   
}
