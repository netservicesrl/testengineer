﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Progetto_Test_WebAPI.Models
{
    public class FileUploadInfo
    {
        //FileName = Path.GetFileName(name),
        //                            FileSize = memoryStream.Length,
        //                            UploadDate = DateTime.Now,
        //                            UploadedBy = "Admin",
        //                            FileContent = memoryStream.ToArray()
       
        public Guid Id { get; set; }
        public string FileName { get; set; }
        public long FileSize { get; set; }

        public DateTime UploadDate { get; set; }

        public string UploadedBy { get; set; }

        public byte[] FileContent { get; set; }
    }
}
