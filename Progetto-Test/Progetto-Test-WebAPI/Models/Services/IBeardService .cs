﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Progetto_Test_WebAPI.Models.Services
{
    public interface IBeardService<T>

    {

        IEnumerable<T> GetAllItems();

        T Add(T newItem);

        T GetById(Guid id);

        void Remove(Guid id);

    }
}
