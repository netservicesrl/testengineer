﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Progetto_Test_WebAPI.Models.Services
{
    public class EmailService
    {
        public async Task Send(EmailItem email)
        {
            //throw new NotImplementedException();
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("in-v3.mailjet.com");

                mail.From = new MailAddress("mobile@net-serv.it");
                mail.To.Add(email.To);
                mail.Subject = email.Subject;
                mail.Body = email.Body;

                SmtpServer.Port = 587;
                string ApiKey = "1e80401204e5d99314454b7e54f6d9d0";
                string SecretKey = "fcc19c8d1718b6a01d5f4c72ce000856";
                SmtpServer.Credentials = new System.Net.NetworkCredential(ApiKey, SecretKey);
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.EnableSsl = true;

                await SmtpServer.SendMailAsync(mail);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
		}       
    }
}
