﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Progetto_Test_WebAPI.Models
{
    public class EmployeeItem
    {
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Surame { get; set; }
        [Required]
        public string FiscalCode { get; set; }

        public string Email { get; set; }
      
    }
}
