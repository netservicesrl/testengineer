﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Progetto_Test_WebAPI.Models
{
    public class EmailItem
    {
        [Required]
        public string To { get; set; }
		[Required]
        public string Subject { get; set; }
		public string Body { get; set; }
    }
}
