﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Progetto_Test_WebAPI.Models
{
    public class OrganizationItem

    {

        public Guid Id { get; set; }

        [Required]

        public string Name { get; set; }

        public IList<string> Addresses { get; set; }

        public IList<string> Contacts { get; set; }

        public int EmployeeCount { get; set; }


    }
}
