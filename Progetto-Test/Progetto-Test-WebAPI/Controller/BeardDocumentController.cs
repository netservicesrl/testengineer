﻿using Microsoft.AspNetCore.Mvc;
using Progetto_Test_WebAPI.Models;
using Progetto_Test_WebAPI.Models.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Progetto_Test_WebAPI.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class BeardDocumentController : ControllerBase
    {
        private readonly IBeardService<DocumentItem> _service;
        public BeardDocumentController(IBeardService<DocumentItem> service)
        {
            this._service = service;
        }
        // GET: api/<BeardDocumentController>
        [HttpGet]
        public IActionResult Get()
        {
            var items = _service.GetAllItems();
            return Ok(items);
        }
        // GET api/<BeardDocumentController>/5
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var item = _service.GetById(id);

            if (item == null)

            {

                return NotFound();

            }

            return Ok(item);

        }

        // POST api/<BeardDocumentController>
        [HttpPost]
        public IActionResult Post([FromBody] DocumentItem value)

        {

            if (!ModelState.IsValid)

            {

                return BadRequest(ModelState);

            }

            var item = _service.Add(value);

            return CreatedAtAction("Get", new { id = item.Id }, item);

        }

        // PUT api/<BeardDocumentController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        // DELETE api/<BeardDocumentController>/5
        [HttpDelete("{id}")]
        public IActionResult Remove(Guid id)

        {

            var existingItem = _service.GetById(id);

            if (existingItem == null)

            {

                return NotFound();

            }

            _service.Remove(id);

            return NoContent();

        }
    }
}
