﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Progetto_Test_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Progetto_Test_WebAPI.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private string _errorMessage { get; set; }
        public async Task<FileUploadResponse> UploadFiles(IFormFile f)
        {
            FileUploadResponseData uploadedFile = null;

            this._errorMessage = "";
            try
            {
                //foreach (var f in files)
                //{
                string name = f.FileName.Replace(@"\\\\", @"\\");
                if (f.Length > 0)
                {
                    var memoryStream = new MemoryStream();
                    // Check file name is valid
                    if (f.FileName.Length == 0)
                    {
                        uploadedFile = new FileUploadResponseData()
                        {
                            Id = Guid.Empty,
                            Status = "ERROR",
                            FileName = Path.GetFileName(name),
                            ErrorMessage = "File " + f
                        + " failed to upload. File name is invalid."
                        };
                        this._errorMessage = "ERROR";

                    }
                    try
                    {
                        await f.CopyToAsync(memoryStream);
                        // Upload check if the file is less than 2mb!
                        if (memoryStream.Length < 2097152)
                        {
                            var file = new FileUploadInfo()
                            {
                                Id = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c288"),
                                FileName = Path.GetFileName(name),
                                FileSize = memoryStream.Length,
                                UploadDate = DateTime.Now,
                                UploadedBy = "Admin",
                                FileContent = memoryStream.ToArray()
                            };
                            //_db.UploadedFiles.Add(file);
                            //await _db.SaveChangesAsync();
                            uploadedFile = new FileUploadResponseData()
                            {
                                Id = file.Id,
                                Status = "OK",
                                FileName = Path.GetFileName(name),
                                ErrorMessage = "",
                            };

                            if (this._errorMessage != "ERROR")
                                this._errorMessage = "OK";
                        }
                        else
                        {
                            uploadedFile = new FileUploadResponseData()
                            {
                                Id = Guid.Empty,
                                Status = "ERROR",
                                FileName = Path.GetFileName(name),
                                ErrorMessage = "File " + f
                            + " failed to upload. File sized exceeds limit."
                            };
                            this._errorMessage = "ERROR";
                        }
                    }
                    finally
                    {
                        memoryStream.Close();
                        memoryStream.Dispose();
                    }
                }
                //}
                return new FileUploadResponse() { Data = uploadedFile, ErrorMessage = "" };
            }
            catch (Exception ex)
            {
                return new FileUploadResponse() { ErrorMessage = ex.Message.ToString() };
            }
        }
        public async Task<IFormFile> DownloadFile (Guid Id)
        {
            try
            {

            }
            catch(Exception ex)
            { }
            return null;
        }
    }

    public class FileUploadResponse
    {
        public FileUploadResponseData Data { get; set; }
        public string ErrorMessage { get; set; }
    }
}
