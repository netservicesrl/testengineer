﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Progetto_Test_WebAPI.Models;
using Progetto_Test_WebAPI.Models.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Progetto_Test_WebAPI.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
		private readonly EmailService _service;
		public EmailController(EmailService service)
		{
			this._service = service;
		}
		public IActionResult Send( EmailItem item)
        {
			try
			{
				if (item == null || !ModelState.IsValid)
				{
					return BadRequest(ModelState);
				}

				_service.Send(item).GetAwaiter();
				return Ok(true);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
    }

}
